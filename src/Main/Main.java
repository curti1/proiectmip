package Main;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.eclipse.persistence.internal.jpa.metadata.sequencing.GeneratedValueMetadata;

import model.Animal;
import model.Appointment;
import model.Medicalstaff;
import util.DatabaseUtil;
import CRUD.CRUD_animal;
//import controllers.MainController.OnCompleteListener;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

interface OnCompleteListener {
	public void OnCompleteListener() throws Exception;
}

class LongRunningTask implements Runnable {
	
	private OnCompleteListener onCompleteListener;
	
	public OnCompleteListener getOnCompleteListener() {
		return onCompleteListener;
	}
	
	public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(1*1000);
			onCompleteListener.OnCompleteListener();				
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

public class Main extends Application{
		

	
		public static void main(String[] args) throws Exception {
		
		System.out.println("started app\n");
	    LongRunningTask longRunningTask = new LongRunningTask();
	    longRunningTask.setOnCompleteListener(new OnCompleteListener() {
	    	@Override
	    	public void OnCompleteListener() throws Exception{
	    		System.out.println("meri");
	    		//launch(args);
	    	}
	    });
	    System.out.println("Finished loading app\n");
		//OnCompleteListener onCompleteListener;
	    	longRunningTask.run();		
		launch(args);
		
		DatabaseUtil dbUtil= new DatabaseUtil();
		dbUtil.setUp();
		/*
		
		Date date = new SimpleDateFormat( "dd-MM-yyyy HH:mm" ).parse( "19-11-2018 21:20" );
		Appointment app = new Appointment();//int idappointment, Date appointmentDate, String emergencyGrade, 	Animal animal, Medicalstaff medicalstaff)
		//app.setAnimal(bob);
		//app.setMedicalstaff(medi);
		//app.setDate(date);
		app.setType("emergency");
		
		dbUtil.startTransaction();
		
		dbUtil.commitTransaction();
		dbUtil.saveAppointment(app);
	//	dbUtil.commitTransaction();
	//	dbUtil.printAllAnimalsFromDB();
	//	dbUtil.printAllMedicalstaffFromDB();
		dbUtil.printAllAppointmentsFromDB();
		dbUtil.closeEntityManager();
		
		/*	
		DatabaseUtil dbUtil= new DatabaseUtil();
		String [] optionsAnimal = {"1. Create Animal", "2. Read Animal", "3. update Animal", "4. Delete Animal"};
		 
		
		dbUtil.setUp();
		
		//CRUD_animal.delete(1);
		
		
		//dbUtil.startTransaction();
		
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
		*/
		
		// till here
		
		
		//CRUD_animal.read(1);
		//CRUD_animal.delete(1);

		/*
		 * dbUtil.setUp();
		 /*
		dbUtil.startTransaction();
		dbUtil.readAnimal(2);

		dbUtil.closeEntityManager();
		 */
		 //printMenu(optionsAnimal);
		 
		/*
		while(true) {
			
	        printMenu(optionsAnimal);
	        Scanner scan = new Scanner(System.in);
	        int option = scan.nextInt();
		}*/
		
		/*
		Animal2 bob = new Animal2();
		bob.setIdAnimal(4);
		bob.setName("Bob");
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(bob);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
		*/
		/*
		DatabaseUtil dbUtil= new DatabaseUtil();
		dbUtil.setUp();
		
		Animal bob = new Animal();
		bob.setName("testsaccasdd");
		bob.setOwnername("Vladads");
		bob.setOwnerrphonenumber("085682421121");
		bob.setRace("bischon maltezos");
		bob.setSpecies("cannis Cannis");
		
		Medicalstaff medi = new Medicalstaff();
		medi.setName("Dr drege");
		medi.setSurname("shader");
		Date date = new SimpleDateFormat( "yyyyMMdd" ).parse( "20100520" );
		Appointment app = new Appointment( date, "just a scratch", bob, medi);//int idappointment, Date appointmentDate, String emergencyGrade, 	Animal animal, Medicalstaff medicalstaff)
		//app.setAnimal(bob);
		//app.setMedicalstaff(medi);

		
		dbUtil.startTransaction();
		dbUtil.saveAnimal(bob);
		dbUtil.saveMedicalStaff(medi);
		
		//dbUtil.commitTransaction();
		dbUtil.saveAppointment(app);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.printAllMedicalstaffFromDB();
		dbUtil.printAllAppointmentsFromDB();
		dbUtil.closeEntityManager();
		*/
		/*
		DatabaseUtil dbUtil= new DatabaseUtil();
		dbUtil.setUp();
		
		Animal bob = new Animal();
		bob = dbUtil.getAnimalById(2);
		
		Medicalstaff medi = new Medicalstaff();
		medi = dbUtil.getMedicalstaffById(2);
		
		Date date = new SimpleDateFormat( "yyyyMMdd" ).parse( "20100520" );
		Appointment app = new Appointment( date, "just a scratch", bob, medi);//int idappointment, Date appointmentDate, String emergencyGrade, 	Animal animal, Medicalstaff medicalstaff)
		//app.setAnimal(bob);
		//app.setMedicalstaff(medi);

		
		dbUtil.startTransaction();
		
		//dbUtil.commitTransaction();
		dbUtil.saveAppointment(app);
		dbUtil.commitTransaction();
	//	dbUtil.printAllAnimalsFromDB();
	//	dbUtil.printAllMedicalstaffFromDB();
		System.out.println("asdasdsadasdasdsadasdawddsffcesdfffcdrsfcdfcds");
		dbUtil.printAllAppointmentsFromDB();
		dbUtil.closeEntityManager();
		*/
	}
		
	    public static void printMenu(String [] options){
	         
	        for(String str : options){
	            System.out.println(str);
	        }
	    }

		@Override
		public void start(Stage primaryStage) throws Exception {
			//log in
			try {
				AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/controllers/LogIn.fxml"));
				Scene scene = new Scene(root, 250, 270);
				Stage stage = new Stage();
				stage.setScene(scene);
				stage.show();
			}
			catch(Exception e) {
			e.printStackTrace();
			}	
					
		}

}
