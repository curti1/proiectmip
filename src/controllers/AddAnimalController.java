package controllers;

import java.awt.Component;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import Logger.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Animal;
import util.DatabaseUtil;

public class AddAnimalController implements Initializable {
	
	@FXML
	private TextField p_name;
	
	@FXML
	private TextField p_owner;
	
	@FXML
	private TextField p_phone;
	
	@FXML
	private TextField p_race;

	@FXML
	private TextField p_species;
	
	@FXML
	private Button p_ptnRegister;
	
	@FXML
	private ImageView p_animalPicture;
	
	
	String defaultImage = "file:///C:/Users/Andrei/eclipse-workspace/AnimalHospital_Take7/img/default.png";
	
	void showErrorMessage(String text, String title){
		Component panel = null;
		JOptionPane.showMessageDialog(panel, text, title, JOptionPane.ERROR_MESSAGE);
	}
	
	boolean allFieldsAreFilled()
	{

		if(p_name.getText().equals(""))
		{
			showErrorMessage("Name field cannot be null", "Error");
			return false;
		}
		
		if(p_owner.getText().equals(""))
		{
			showErrorMessage("Owner field cannot be null", "Error");
			return false;
		}
		
		if(p_phone.getText().equals(""))
		{
			showErrorMessage("Phone field cannot be null", "Error");
			return false;
		}
		
		if(p_race.getText().equals(""))
		{
			showErrorMessage("Race field cannot be null", "Error");
			return false;
		}
		
		if(p_species.getText().equals(""))
		{
			showErrorMessage("Species field cannot be null", "Error");
			return false;
		}
		return true;	
	}
	
	public void register() {
		
		if(!allFieldsAreFilled())
			return;
		
		Animal animal = new Animal();
		animal.setName(p_name.getText());
		animal.setOwnername(p_owner.getText());
		animal.setOwnerrphonenumber("0758963152");//p_phone.getText()
		animal.setSpecies(p_species.getText());
		animal.setRace(p_race.getText());
		
		try {
		DatabaseUtil dbUtil= new DatabaseUtil();
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animal);
		dbUtil.commitTransaction();
		dbUtil.closeEntityManager();
		}
		catch (Exception e) {
			Component panel = null;
			JOptionPane.showMessageDialog(panel, "Database error", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public Optional<String> getExtension(String filename) {
	    return Optional.ofNullable(filename)
	      .filter(f -> f.contains("."))
	      .map(f -> f.substring(filename.lastIndexOf(".") + 1));
	}
	
	boolean isSupportedFormat(String extension) {
		
		return false;
	}
	
	@FXML
	public void selectAnimalImage() {
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		File selectedFile = fileChooser.showOpenDialog(null);
		
	    
		if(selectedFile != null) {
			if(getExtension(selectedFile.getName()).equals("png") || getExtension(selectedFile.getName()).equals("jpg")){
				p_animalPicture.setImage(new Image(selectedFile.getAbsolutePath()));
			}
			else {
				showErrorMessage("invalid file type", "File type error");
			}
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		Logger.getInstance().addToLog("AddAnimal window opened");
		
		p_animalPicture.setImage(new Image(defaultImage));
	}

}
