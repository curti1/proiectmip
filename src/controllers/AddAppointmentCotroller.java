package controllers;

import java.awt.Component;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import org.eclipse.persistence.jpa.jpql.parser.DateTime;

import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import model.Animal;
import model.Appointment;
import model.Medicalstaff;
import util.DatabaseUtil;
import Logger.Logger;

public class AddAppointmentCotroller implements Initializable{

	@FXML
	private DatePicker p_date;
	
	@FXML
	private ComboBox<Integer> p_hours;
	
	@FXML
	private ComboBox<Integer> p_minutes;
	
	@FXML
	private ComboBox<Animal> p_selectAnimal;
	
	@FXML
	private ComboBox<Medicalstaff> p_selectMedicalstaff;
	
	@FXML
	private Button p_btnSave;
	
	@FXML
	private ComboBox<String> p_selectAppointmentType;
	
	public void restrictDatePicker(DatePicker datePicker, LocalDate minDate) {
	    final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
	        @Override
	        public DateCell call(final DatePicker datePicker) {
	            return new DateCell() {
	                @Override
	                public void updateItem(LocalDate item, boolean empty) {
	                    super.updateItem(item, empty);
	                     if (item.isBefore(minDate)) {
	                        setDisable(true);
	                        setStyle("-fx-background-color: #ffc0cb;");
	                    }
	                }
	            };
	        }
	    };
	    datePicker.setDayCellFactory(dayCellFactory);
	}
	
	public <T> ObservableList<T> convertListToObservableList(List<T> list) {
		ObservableList<T> info = FXCollections.observableArrayList();
		for (T item : list) {
			info.add(item);
		}
		return info;
	}
	
	
	
	public void populateComboBoxAnimal() throws ParseException {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.beginTransaction();
		
		List<Animal> animalDBList = (List<Animal>) db.animalList();
		ObservableList<Animal> animalNamesList = convertListToObservableList(animalDBList);
		
		try {
			
			if(animalNamesList.get(0) != null)
				System.out.println("animalNamesList is not null");
			
			p_selectAnimal.setItems(animalNamesList);
	
		 StringConverter<Animal> converter = new StringConverter<Animal>() {
	            @Override
	            public String toString(Animal animal) {
	                return animal.getIdAnimal() + ":	" + animal.getName();
	            }
	            @Override
	            public Animal fromString(String id) {
	                return animalNamesList.stream()
	                        .filter(item -> String.valueOf(item.getIdAnimal()).equals(id))
	                        .collect(Collectors.toList()).get(0);
	            }
	            
	        };
	        
	        p_selectAnimal.setConverter(converter);

		}catch(NullPointerException e) {
			e.printStackTrace();
		}

		db.stop();
	}
	
	
	
	public void populateComboBoxMedicalStaff() throws ParseException {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.beginTransaction();
		
		List<Medicalstaff> medicalstaffDBList = (List<Medicalstaff>) db.medicalstaffList();
		ObservableList<Medicalstaff> medicalstaffNamesList = convertListToObservableList(medicalstaffDBList);
		
		try {
			
			if(medicalstaffNamesList.get(0) != null)
				System.out.println("meddicalstaffNamesList is not null");
			
			p_selectMedicalstaff.setItems(medicalstaffNamesList);
	
			 StringConverter<Medicalstaff> converter = new StringConverter<Medicalstaff>() {
		            @Override
		            public String toString(Medicalstaff medicalstaff) {
		                return medicalstaff.getIdMedicalstaff() + ":	" + medicalstaff.getName();
		            }
		            @Override
		            public Medicalstaff fromString(String id) {
		                return medicalstaffNamesList.stream()
		                        .filter(item -> String.valueOf(item.getIdMedicalstaff()).equals(id))
		                        .collect(Collectors.toList()).get(0);
		            }
	            
	        };
	        
	        p_selectMedicalstaff.setConverter(converter);

		}catch(NullPointerException e) {
			e.printStackTrace();
		}

		db.stop();
	}
	
	void populateComboBoxHour() {
		ObservableList<Integer> hourList = FXCollections.observableArrayList();
		
		for(Integer h=0; h<=23; h++) {
			hourList.add(h);
		}
		
		p_hours.setItems(hourList);
	}
	
	void populateComboBoxMinutes() {
		ObservableList<Integer> minutesList = FXCollections.observableArrayList();
		
		for(Integer m=0; m<60; m+=5) {
			minutesList.add(m);
		}
		
		p_minutes.setItems(minutesList);
	}
	
	
	@FXML
	void btnSavePressed() {
		//Todo: add tests
		//of(int year, Month month, int dayOfMonth, int hour, int minute)
		//LocalDateTime ldt = LocalDateTime.of(p_date.getValue().getYear(), p_date.getValue().getMonth(), 
		//		p_date.getValue().getDayOfMonth(), p_hours.getValue(), p_minutes.getValue());
		
	
		
		@SuppressWarnings("deprecation")
		Date date = new Date(p_date.getValue().getYear() - 1900, p_date.getValue().getMonth().getValue() - 1, 
						p_date.getValue().getDayOfMonth(), p_hours.getValue(), p_minutes.getValue());
		
		Animal animal = new Animal();
		Medicalstaff medicalstaff = new Medicalstaff();
		
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.beginTransaction();
		
		animal = db.getAnimalById(p_selectAnimal.getValue().getIdAnimal());
		medicalstaff = db.getMedicalstaffById(p_selectMedicalstaff.getValue().getIdMedicalstaff());
		
		Appointment appointment = new Appointment(date, animal, medicalstaff, p_selectAppointmentType.getValue());
		
		db.saveAppointment(appointment);
		db.commitTransaction();
		db.closeEntityManager();
		
		Stage stage = (Stage) p_btnSave.getScene().getWindow();
		
		Object[] options = {"OK"};
		 int result = JOptionPane.showOptionDialog(null,
                 "Appointment saved ","SUCCESS!",
                 JOptionPane.PLAIN_MESSAGE,
                 JOptionPane.QUESTION_MESSAGE,
                 null,
                 options,
                 options[0]);
		 
		 if(result == JOptionPane.OK_OPTION) {
			    stage.close();
			    System.out.println("close");
		 }
	}
	
	public void populateComboBoxType() {
		ObservableList<String> types =  FXCollections.observableArrayList();
		types.setAll("emergency", "routine check", "surgery");
		
		p_selectAppointmentType.setItems(types);
	}
	
	public void initialize(URL arg0, ResourceBundle arg1) {

		Logger.getInstance().addToLog("AddAppointment window opened");
		
		restrictDatePicker(p_date, LocalDate.now() );getClass();
		try {
			populateComboBoxMedicalStaff();
			populateComboBoxAnimal();
			populateComboBoxHour();
			populateComboBoxMinutes();
			populateComboBoxType();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		catch (NullPointerException e){
			System.out.println("Null Pointer Exception");
			Logger.getInstance().addToLog("Null Pointer Exception in AddAppointment initializer");
		}
	}
	
		
}


