package controllers;

import java.io.IOException;
import java.net.URL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import Logger.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Animal;
import model.Appointment;
import model.MedicalHistory;
import util.DatabaseUtil;
import model.MedicalHistorySimplified;

/**
 * This is the entry point of the app
 * @author Andrei
 *
 */

public class MainController implements Initializable {
	
	String defaultImage = "file:///C:/Users/Andrei/eclipse-workspace/AnimalHospital_Take7/img/default.png";
	static //LocalDateTime currentDate = LocalDateTime.now();
	Calendar cal = Calendar.getInstance();
	DateFormat dateFormatDMY = new SimpleDateFormat("dd-MM-yyyy");
	DateFormat dateFormatYMDHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormatYMD = new SimpleDateFormat("yyyy-MM-dd");
	DateFormat dateFormatDMYHM = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	DateFormat dateFormatHM = new SimpleDateFormat("HH:mm");
	
	static Date selectedDate = cal.getTime();
	
	//String selectedDate = dateFormatDMY.format(cal.getTime());
	   
	@FXML
	private ListView<Appointment> listView;

	@FXML
	private Text p_name;

	@FXML
	private Text p_age;

	@FXML
	private Text p_species;

	@FXML
	private Text p_owner;

	@FXML
	private Text p_phone;
	
	@FXML
	private ImageView p_image;
	
	@FXML
	private Text p_textDate;
	
	@FXML
	private Button p_btnNavigateDateLeft;
	
	@FXML
	private Button p_btnNavigateDateRight;
	
	@FXML
	private TableView<MedicalHistorySimplified> p_tableMedicalHistory;
	
	@FXML
	private MenuItem m_addAnimal;

	Appointment clickedApp = null;

	/**
	 * handles the event when user clicks an entry in the appointment list
	 */
	public void handle(){
		
		clickedApp = (Appointment)listView.getSelectionModel().getSelectedItem();
	
		
		try {
			p_name.setText(clickedApp.getAnimal().getName());
		}catch(NullPointerException e) {
			
		}
		try {
			p_owner.setText("Owner " + clickedApp.getAnimal().getOwnername());
		}catch(NullPointerException e) {
			
		}
		try {
			p_phone.setText("Phone: " + clickedApp.getAnimal().getOwnerrphonenumber());
		}catch(NullPointerException e) {
			
		}
		
		
		try {
			p_image.setImage(new Image("file:///" + clickedApp.getAnimal().getPicture()));
		}catch(NullPointerException e){
			p_image.setImage(new Image(defaultImage));
		}
		
		populateMedicalHistoryTable();
	}
	
	/**
	 * populates the medicalHistory table with the data correspoonding to the selectected animal
	 */
	public void populateMedicalHistoryTable(){
		p_tableMedicalHistory.setEditable(true);
		
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.beginTransaction();
		
		List<MedicalHistory> medHistoryList = (List<MedicalHistory>) db.medicalHistoryListByAnimal(clickedApp.getAnimal().getIdAnimal());
		
		if(medHistoryList.isEmpty()) {
			System.out.println("no medH");
			return;
		}
			
		ObservableList<MedicalHistorySimplified> list = FXCollections.observableArrayList() ;
		
		for(MedicalHistory medH : medHistoryList) {
			MedicalHistorySimplified medHS = new MedicalHistorySimplified();
			medHS.simplifyMedicalHistory(medH);
			list.add(medHS);
		}
		
		for(MedicalHistorySimplified medHS : list) {
			System.out.println(medHS.getDescription());
		}
			
		p_tableMedicalHistory.setItems(list);

		db.stop();
	}

	
	private static Map<String, String> appointmentTypeColors= new HashMap<>();
	
	private static void initializeAppoinmentTypeColors() {
		appointmentTypeColors.put("emergency", "red");
		appointmentTypeColors.put("routine check", "yellowgreen");
		appointmentTypeColors.put("surgery", "yellow");
	}
	
	@SuppressWarnings("unchecked")
	public void setUpMedicalHistoryTable() {
		TableColumn<MedicalHistorySimplified, String> dateColumn = new TableColumn<MedicalHistorySimplified, String>("Date");
		dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
		

		TableColumn<MedicalHistorySimplified, String> diagnosisColumn = new TableColumn<MedicalHistorySimplified, String>("Diagnosis");
		diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
		
		TableColumn<MedicalHistorySimplified, String> descriptionColumn = new TableColumn<MedicalHistorySimplified, String>("Description");
		descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
		descriptionColumn.setMinWidth(p_tableMedicalHistory.getWidth() - dateColumn.getWidth() - diagnosisColumn.getWidth());
		
		p_tableMedicalHistory.getColumns().addAll(dateColumn, diagnosisColumn, descriptionColumn);
	}


	public void populateMainListView() throws ParseException {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.beginTransaction();
		
		List<Appointment> appointmentDBList = (List<Appointment>) db.appointmentListByDate(dateFormatYMD.format(selectedDate));
		ObservableList<Appointment> appointmentNamesList = getAppointmentInfo(appointmentDBList);
		listView.setItems(appointmentNamesList.sorted());
		
		listView.setCellFactory(new Callback<ListView<Appointment>, ListCell<Appointment>>() {
		    @Override
		    public ListCell<Appointment> call(ListView<Appointment> param) {
		         ListCell<Appointment> cell = new ListCell<Appointment>() {
		        	 
		             @Override
		            protected void updateItem(Appointment item, boolean empty) {
		                super.updateItem(item, empty);
		                if(item != null && item.getAnimal() != null) {		                	
		                    setText(item.getAnimal().getName() + " " + dateFormatHM.format(item.getDate()) + " " + item.getType());
		                    setStyle("-fx-background-color: " + appointmentTypeColors.get(item.getType()));                
		                } else {
		                    setText(null);
		                }
		            }
		         };
		        return cell;
		    }

		});
		
		listView.refresh();
		db.stop();
	}

	public void navigateDateLeft(){
		navigateDate(-1);
		
		p_textDate.setText(dateFormatDMY.format(selectedDate));
		
		try {
			populateMainListView();
			p_image.setImage(new Image(defaultImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void navigateDateRight(){
		navigateDate(1);
		
		p_textDate.setText(dateFormatDMY.format(selectedDate));
		
		try {
			populateMainListView();
			p_image.setImage(new Image(defaultImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void navigateDate(int numberOfDays) {
		Calendar auxCal = Calendar.getInstance();
		auxCal.setTime ( selectedDate ); 
		auxCal.add(Calendar.DATE, numberOfDays);
		selectedDate = auxCal.getTime();
	}

	
	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals) {
			names.add(a.getName());
		}
		return names;
	}

	public ObservableList<Appointment> getAppointmentInfo(List<Appointment> appointments) {
		ObservableList<Appointment> info = FXCollections.observableArrayList();
		for (Appointment a : appointments) {
			info.add(a);
		}
		return info;
	}
	
	@FXML
	private void openAddAnimalWindow(final ActionEvent event) throws IOException {
		
		try {
			FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("AddAnimal.fxml"));
			Parent root2 = (Parent) fxmlLoader2.load();
			
			Stage stage2 = new Stage();
			stage2.setScene(new Scene(root2));
			stage2.show();
		}catch(Exception e) {
			System.out.println("The AddAnimal window cannot be opened!");
		}
		
	}
	
	@FXML
	private void openAddAppointmentWindow(final ActionEvent event) throws IOException {

	//	try {
			FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("AddAppointment.fxml"));
			Parent root2 = (Parent) fxmlLoader2.load();
			
			Stage stage2 = new Stage();
			stage2.setScene(new Scene(root2));
			stage2.show();
	//	}catch(Exception e) {
	//		System.out.println("The AddAppointment window cannot be opened!");
		//}
		
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
			
		Logger myLogger = Logger.getInstance();
		myLogger.addToLog("Application started");
		
		p_textDate.setText(dateFormatDMY.format(selectedDate));
		initializeAppoinmentTypeColors();
		
		try {
			populateMainListView();
			setUpMedicalHistoryTable();
			p_image.setImage(new Image(defaultImage));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
