package controllers;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.io.OutputStreamWriter;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import Logger.Logger;
import ClientServerConnection.Client;
import ClientServerConnection.Server;

public class LogInController implements Initializable{
	
	@FXML
	private TextField p_username;
	
	@FXML
	private PasswordField p_password;
	
	@FXML
	private Button p_btnLogIn;
	
	@FXML
	private Text p_txtMessage;


	
	@FXML
	void attemptLogIn() {
		
		if(p_username.getText() == "") {
			p_txtMessage.setText("Type a username");
			return;
		}
		
		if(p_password.getText() == "") {
			p_txtMessage.setText("Type a password");
			return;
		}
		
		//the following block is used because server logging in does not work
		//Todo: fix the problem and delete the block
		{
			Stage primaryStage = new Stage();
			try {
				AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
				Scene scene = new Scene(root, 800,700);
				primaryStage.setScene(scene);
				primaryStage.show();
				
				//close current Stage
				Stage login = (Stage) p_btnLogIn.getScene().getWindow();
				login.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
			return;
		}
		
		/*
		Server server = new Server();
		server.run();
		Client client = new Client();
		boolean isValidUser = false;
		
		
		try {
			isValidUser = client.start(p_username.getText(), p_password.getText());
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(isValidUser){
			Stage stage = (Stage) p_btnLogIn.getScene().getWindow();
			stage.hide();
				
			Stage primaryStage = new Stage();
			try {
				AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
				Scene scene = new Scene(root, 800,800);
				primaryStage.setScene(scene);
				primaryStage.show();
				
				Stage login = (Stage) p_btnLogIn.getScene().getWindow();
				login.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
				
		}
		else {
			p_txtMessage.setText("Invalid username or password");
		}
		*/
	}

	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Logger.getInstance().addToLog("=== NEW SESSION ===" + System.lineSeparator() + "Log in window opened");
		
		p_txtMessage.setText("");
	}

}
