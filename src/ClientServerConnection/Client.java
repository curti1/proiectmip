package ClientServerConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	
	Socket socket;
	String hostName = "localhost";
	int portNumber = 5000;
	
	public boolean start(String username, String password) throws UnknownHostException, IOException {
        //Create socket connection
        socket = new Socket("localhost", 6000);

        //create printwriter for sending login to server
        PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

        //send user name to server
        output.println(username);

        //send password to server
        output.println(password);
        output.flush();

        //create Buffered reader for reading response from server
        BufferedReader read = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //read response from server
        String response;
        try {
			response = read.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response = "false";
		}
        
        if(response.compareTo("true") == 0) {
        	return true;
        }
        return false;
    }

}
