package ClientServerConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * the server class manages the server 
 * @author Andrei
 *
 */

public class Server extends Thread{
	
	/**
	 * the server is used for medical staff authentification, so it contains the doctorsDatabase of usernames and passwords
	 */	
	private static Map<String, String> doctorsDatabase = new HashMap();
	
	private static void initializeDoctorsDatabase() {
		doctorsDatabase.put("Dr Dre", "852456");
		doctorsDatabase.put("Dr House", "Vicodin");
		doctorsDatabase.put("a", "a");
	}
	
	public Server() {
		initializeDoctorsDatabase();
	}
	
	/**
	 * Check if credentials given by the param are valid
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean isValidLogIn(String username, String password) {
		if (doctorsDatabase.containsKey(username))
			if(doctorsDatabase.get(username).equals(password))
				return true;
		
		return false;
	}
	
	//private ServerSocket serverSocket;
	//private Socket client;
	int portNumber = 5000;
	
	@Override
	public void run() {
		try {
			System.out.println("Server started");
			
			//make connection to client on port specified
			ServerSocket serverSocket = new ServerSocket(4000);
			
			//accept connection from client
	        Socket client = (serverSocket.accept());
	        
	      //open printwriter for writing data to client
	        PrintWriter output = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
	        
	        //open buffered reader for reading data from client
	        BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
	        
	        String username = input.readLine();
	        String password = input.readLine();

	        if(isValidLogIn(username, password)){
	            output.println("Login Succeded");
	        }else{
	            output.println("Login Failed");
	        }
	        output.flush();
	        output.close();
	        
	        serverSocket.close();
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
