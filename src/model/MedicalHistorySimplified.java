package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import model.MedicalHistory;

public class MedicalHistorySimplified  {

	private String date;

	private String description;

	private String diagnosis;

	DateFormat dateFormatDMY = new SimpleDateFormat("dd-MM-yyyy");
	
	public MedicalHistorySimplified() {
	}
	
	public MedicalHistorySimplified(String date, String diagnosis, String description) {
		this.date = date;
		this. diagnosis = diagnosis;
		this.description = description;
	}
	
	public void simplifyMedicalHistory(MedicalHistory medH) {
		//this.date = dateFormatDMY.format( medH.getDate()) ;
		this.date = "today";
		this. diagnosis = medH.getDiagnosis();
		this.description = medH.getDescription();
	}
	
	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiagnosis() {
		return this.diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}



}