package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medicalstaff database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalstaff.findAll", query="SELECT m FROM Medicalstaff m")
public class Medicalstaff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_medicalstaff")
	private int idMedicalstaff;

	private String name;

	private String surname;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalstaff")
	private List<Appointment> appointments;

	//bi-directional many-to-one association to MedicalHistory
	@OneToMany(mappedBy="medicalstaff")
	private List<MedicalHistory> medicalHistories;

	public Medicalstaff() {
	}

	public int getIdMedicalstaff() {
		return this.idMedicalstaff;
	}

	public void setIdMedicalstaff(int idMedicalstaff) {
		this.idMedicalstaff = idMedicalstaff;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalstaff(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalstaff(null);

		return appointment;
	}

	public List<MedicalHistory> getMedicalHistories() {
		return this.medicalHistories;
	}

	public void setMedicalHistories(List<MedicalHistory> medicalHistories) {
		this.medicalHistories = medicalHistories;
	}

	public MedicalHistory addMedicalHistory(MedicalHistory medicalHistory) {
		getMedicalHistories().add(medicalHistory);
		medicalHistory.setMedicalstaff(this);

		return medicalHistory;
	}

	public MedicalHistory removeMedicalHistory(MedicalHistory medicalHistory) {
		getMedicalHistories().remove(medicalHistory);
		medicalHistory.setMedicalstaff(null);

		return medicalHistory;
	}

}