package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the appointment database table.
 * 
 */
@Entity
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_appointment")
	private int idAppointment;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private String type;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="id_animal")
	private Animal animal;

	//bi-directional many-to-one association to Medicalstaff
	@ManyToOne
	@JoinColumn(name="id_medicalstaff")
	private Medicalstaff medicalstaff;

	public Appointment(Date date, Animal animal, Medicalstaff medicalstaff, String type) {
		this.date = date;
		this.animal = animal;
		this.medicalstaff = medicalstaff;
		this.type = type;
	}
	
	public Appointment() {
	}
	
	public int getIdAppointment() {
		return this.idAppointment;
	}

	public void setIdAppointment(int idAppointment) {
		this.idAppointment = idAppointment;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Medicalstaff getMedicalstaff() {
		return this.medicalstaff;
	}

	public void setMedicalstaff(Medicalstaff medicalstaff) {
		this.medicalstaff = medicalstaff;
	}

}