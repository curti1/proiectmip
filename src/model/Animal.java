package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_animal")
	private int idAnimal;

	private String name;

	private String ownername;

	private String ownerrphonenumber;

	private String picture;

	private String race;

	private String species;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="animal")
	private List<Appointment> appointments;

	//bi-directional many-to-one association to MedicalHistory
	@OneToMany(mappedBy="animal")
	private List<MedicalHistory> medicalHistories;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwnername() {
		return this.ownername;
	}

	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}

	public String getOwnerrphonenumber() {
		return this.ownerrphonenumber;
	}

	public void setOwnerrphonenumber(String ownerrphonenumber) {
		this.ownerrphonenumber = ownerrphonenumber;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getRace() {
		return this.race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setAnimal(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setAnimal(null);

		return appointment;
	}

	public List<MedicalHistory> getMedicalHistories() {
		return this.medicalHistories;
	}

	public void setMedicalHistories(List<MedicalHistory> medicalHistories) {
		this.medicalHistories = medicalHistories;
	}

	public MedicalHistory addMedicalHistory(MedicalHistory medicalHistory) {
		getMedicalHistories().add(medicalHistory);
		medicalHistory.setAnimal(this);

		return medicalHistory;
	}

	public MedicalHistory removeMedicalHistory(MedicalHistory medicalHistory) {
		getMedicalHistories().remove(medicalHistory);
		medicalHistory.setAnimal(null);

		return medicalHistory;
	}

}