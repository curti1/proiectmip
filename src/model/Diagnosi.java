package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the diagnosis database table.
 * 
 */
@Entity
@Table(name="diagnosis")
@NamedQuery(name="Diagnosi.findAll", query="SELECT d FROM Diagnosi d")
public class Diagnosi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_diagnosis")
	private int idDiagnosis;

	private String description;

	private String diagnosis;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="id_animal")
	private Animal animal;

	//bi-directional many-to-one association to Medicalstaff
	@ManyToOne
	@JoinColumn(name="id_medicalstaff")
	private Medicalstaff medicalstaff;

	public Diagnosi() {
	}

	public int getIdDiagnosis() {
		return this.idDiagnosis;
	}

	public void setIdDiagnosis(int idDiagnosis) {
		this.idDiagnosis = idDiagnosis;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiagnosis() {
		return this.diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Medicalstaff getMedicalstaff() {
		return this.medicalstaff;
	}

	public void setMedicalstaff(Medicalstaff medicalstaff) {
		this.medicalstaff = medicalstaff;
	}

}