package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the medical_history database table.
 * 
 */
@Entity
@Table(name="medical_history")
@NamedQuery(name="MedicalHistory.findAll", query="SELECT m FROM MedicalHistory m")
public class MedicalHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_diagnosis")
	private int idDiagnosis;

	@Temporal(TemporalType.DATE)
	private Date date;

	private String description;

	private String diagnosis;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="id_animal")
	private Animal animal;

	//bi-directional many-to-one association to Medicalstaff
	@ManyToOne
	@JoinColumn(name="id_medicalstaff")
	private Medicalstaff medicalstaff;

	public MedicalHistory() {
	}

	public int getIdDiagnosis() {
		return this.idDiagnosis;
	}

	public void setIdDiagnosis(int idDiagnosis) {
		this.idDiagnosis = idDiagnosis;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiagnosis() {
		return this.diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Medicalstaff getMedicalstaff() {
		return this.medicalstaff;
	}

	public void setMedicalstaff(Medicalstaff medicalstaff) {
		this.medicalstaff = medicalstaff;
	}

}