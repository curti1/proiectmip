package util;

import java.sql.ResultSet;
import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import javafx.collections.ObservableList;
import model.Animal;
import model.Appointment;
import model.MedicalHistory;
import model.Medicalstaff;
import model.Animal;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp() {
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("AnimalHospital_Take7");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void beginTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() {
		entityManager.close();
	}
	
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.Animal", Animal.class).getResultList();
		for(Animal animal:results) {
			System.out.println("Animal: " + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
	}
	
	public void printAllMedicalstaffFromDB() {
		List<Medicalstaff> results = entityManager.createNativeQuery("Select * from petshop.Medicalstaff", Medicalstaff.class).getResultList();
		for(Medicalstaff medi:results) {
			System.out.println("Doctor: " + medi.getName() + " " + medi.getSurname() + " has ID: " + medi.getIdMedicalstaff());
		}
	}
	
	public void printAllAppointmentsFromDB() {
		List<Appointment> results = entityManager.createNativeQuery("Select * from petshop.Appointment", Appointment.class).getResultList();
		for(Appointment app:results) {
			System.out.println("date: " + app.getDate());
		}
	}
	
	public void readAnimal(int id_animal) {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.Animal where id_animal = " + id_animal, Animal.class).getResultList();
		for(Animal animal:results) {
			System.out.println("Animal: " + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
	}
	
	public void deleteAnimal(int id_animal)	{
		
		entityManager.createNativeQuery("delete from petshop.Animal where id_animal = " + id_animal, Animal.class);
	}
	
	public void stop()
	{
		entityManager.close();
	}
	
	
	public List<Animal> animalList()
	{
		List<Animal> animalList = (List<Animal>) entityManager.createQuery("SELECT a FROM Animal a", Animal.class).getResultList();
		return animalList;
	}
	
	public List<Medicalstaff> medicalstaffList()
	{
		List<Medicalstaff> medicalstaffList = (List<Medicalstaff>) entityManager.createQuery("SELECT a FROM Medicalstaff a", Medicalstaff.class).getResultList();
		return medicalstaffList;
	}
	
	public Animal getAnimalById(int id)
	{
		TypedQuery<Animal> query = entityManager.createQuery("SELECT a FROM Animal a WHERE a.idAnimal = ?1", Animal.class);
		query.setParameter(1, id);
		
		Animal animal = (Animal)query.getSingleResult();
		return animal;
	}
	
	public Medicalstaff getMedicalstaffById(int id)
	{
		//Medicalstaff med = (Medicalstaff) entityManager.createQuery("SELECT a FROM Medicalstaff a WHERE a.idMedicalstaff = " + id, Medicalstaff.class).getResultList();
		TypedQuery<Medicalstaff> query = entityManager.createQuery("SELECT a FROM Medicalstaff a WHERE a.idMedicalstaff = ?1", Medicalstaff.class);
		query.setParameter(1, id);
		
		Medicalstaff med = (Medicalstaff)query.getSingleResult();
		return med;
	}
	
	public List<Appointment> appointmentList()
	{
		List<Appointment> appointmentList = (List<Appointment>) entityManager.createQuery("SELECT a FROM Appointment a", Appointment.class).getResultList();
		return appointmentList;
	}
	
	public List<Appointment> appointmentListByDate(String date)
	{
		List<Appointment> appointmentList = (List<Appointment>) entityManager.createQuery("SELECT a FROM Appointment a WHERE CAST(a.date AS DATE) = " + "'" + date + "'", Appointment.class).getResultList();
		return appointmentList;
	}
	
	public List<MedicalHistory> medicalHistoryListByAnimal(int idAnimal)
	{
		List<MedicalHistory> historyList = (List<MedicalHistory>) entityManager.createQuery("SELECT a FROM MedicalHistory a" , MedicalHistory.class).getResultList();
		return historyList;
	}
	
	public ResultSet medicalHistoryListByAnimal_resultSet(int idAnimal)
	{
		ResultSet historyList = (ResultSet) entityManager.createQuery("SELECT a FROM MedicalHistory a", MedicalHistory.class).getResultList();
		return historyList; //a  WHERE a.id_animal = " + idAnimal 
	}
	
	public void saveAppointment(Appointment appointment) {
		entityManager.persist(appointment);
	}
	
	public void saveMedicalStaff(Medicalstaff medicalstaff) {
		entityManager.persist(medicalstaff);
	}
}
