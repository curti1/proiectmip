package test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ClientServerConnection.Server;

class AppTest {

	@Test
	void isValidUser_ExistingUser_True() {
		Server testServer = new Server();
		boolean isValidUser_shouldBeTrue = testServer.isValidLogIn("a", "a");
		
		assertTrue(isValidUser_shouldBeTrue);
	}
	
	@Test
	void isValidUser_WrongPassword_False() {
		Server testServer = new Server();
		boolean isValidUser_shouldBeFalse = testServer.isValidLogIn("a", "b");
		
		assertFalse(isValidUser_shouldBeFalse);
	}
	
	@Test
	void isValidUser_WrongUsername_False() {
		Server testServer = new Server();
		boolean isValidUser_shouldBeFalse = testServer.isValidLogIn("b", "a");
		
		assertFalse(isValidUser_shouldBeFalse);
	}
	
	@Test
	void isValidUser_WrongUsernameAndPassword_False() {
		Server testServer = new Server();
		boolean isValidUser_shouldBeFalse = testServer.isValidLogIn("0", "1");
		
		assertFalse(isValidUser_shouldBeFalse);
	}
}
