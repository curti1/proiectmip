package CRUD;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import model.Animal;
import util.DatabaseUtil;

public class CRUD_animal {
	
	static DatabaseUtil dbUtil= new DatabaseUtil();
	
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public static void create(String name)throws Exception
	{
		Animal bob = new Animal();
		bob.setName(name);
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(bob);
		dbUtil.commitTransaction();
		dbUtil.closeEntityManager();
	}
	
	public static void read(int id_animal) throws Exception
	{
		DatabaseUtil dbUtil= new DatabaseUtil();
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.readAnimal(id_animal);

		dbUtil.closeEntityManager();
	}
	
	public static void delete(int id_animal) throws Exception
	{
		
		//dbUtil.setUp();
		dbUtil.startTransaction();
		//dbUtil.deleteAnimal(id_animal);
		Animal animalToDelete = new Animal() ;
		try { entityManager.find(Animal.class, id_animal);
		}catch (NullPointerException e){
			System.out.println("The animal is not in the database");
			return;
		}
		
		entityManager.remove(animalToDelete);
		dbUtil.commitTransaction();
		//dbUtil.closeEntityManager();
	}

}
